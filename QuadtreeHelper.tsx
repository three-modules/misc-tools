import Quadtree from "@timohausmann/quadtree-js"
import { DataSample } from "../misc-utils/middleware/DataSources";

export const QuadtreeHelper = ({ currentNode, rootNode, displayRatio = 1 }: { currentNode: Quadtree, rootNode: Quadtree, displayRatio: number }) => {
    const mapOrig = rootNode.bounds;
    // console.log(currentNode.bounds);
    const bounds = currentNode.bounds
    const width = bounds.width * displayRatio;
    const height = bounds.height * displayRatio;
    const left = (bounds.x - rootNode.bounds.x) * displayRatio;
    const top = (bounds.y - rootNode.bounds.y) * displayRatio;
    const dataSample = currentNode.objects[0];
    let bg = "none";
    let borderStyle = "1px solid black";
    if (dataSample instanceof DataSample){ 
        bg = dataSample.dataPoint.y ? "#00ff0025" : "#ff000000";
        // borderStyle = dataSample.elev ? "1px solid green": "1px solid red"
        borderStyle = dataSample.dataPoint.y ? "none": "1px solid red";
    }

    return (<>
        {currentNode.nodes.length ? currentNode.nodes.map((childNode, i) => <QuadtreeHelper key={`node_lvl${childNode.level}_i${i}`} currentNode={childNode} rootNode={rootNode} displayRatio={displayRatio} />) :
            <div style={{ width: width, height: height, border: borderStyle, position: "absolute", zIndex: 2, bottom: top, left: left, background: bg }} />}
    </>)
} 