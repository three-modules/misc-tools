import * as THREE from 'three'
import React, { Suspense, useRef, useState } from 'react'
import { Canvas, useFrame, useLoader } from '@react-three/fiber'
import { TextureLoader } from 'three';
// import { Assets } from '../three-resources/assets';


export const TextureSphericalDisp = () => {
  const [
    colorMap,
    displacementMap,
    normalMap,
    roughnessMap,
    aoMap
  ] = useLoader(TextureLoader, [
    // Assets.Images.Tiles087.getColor(),
    // Assets.Images.Tiles087.getNormal(),
    // Assets.Images.Tiles087.getRoughness(),
    // Assets.Images.Tiles087.getAmbiantOcclusion()
  ]);
  // const [
  //   colorMap,
  //   displacementMap,
  //   normalMap,
  //   roughnessMap,
  //   aoMap
  // ] = useTexture([
  //   name("Color"),
  //   name("Displacement"),
  //   name("Normal"),
  //   name("Roughness"),
  //   name("AmbientOcclusion")
  // ]);
  return (
    <>
      <ambientLight intensity={0.2} />
      <directionalLight />
      <mesh>
        {/* Width and height segments for displacementMap */}
        <sphereBufferGeometry args={[1, 100, 100]} />
        <meshStandardMaterial
          displacementScale={0.2}
          map={colorMap}
          displacementMap={displacementMap}
          normalMap={normalMap}
          roughnessMap={roughnessMap}
          aoMap={aoMap}
        />
      </mesh>
    </>
  );
}


function TextureFlatDisp({ texture, animate = false, ...props }) {

  // This reference will give us direct access to the mesh
  const mesh: any = useRef();

  // Rotate mesh every frame, this is outside of React without overhead
  useFrame(({ clock }) => {
    if (animate) {
      mesh.current.position.x = Math.sin(clock.elapsedTime);
      mesh.current.position.y = Math.cos(clock.elapsedTime);
    }
  });

  return (
    <mesh position={[0,0,3.7]} ref={mesh}>
      <planeGeometry args={[1,]} />
      <meshBasicMaterial map={texture}>
        {/* {dataTex} */}
        {/* {!texture && <dataTexture
          args={[data, size, size, THREE.RGBFormat]}
          attach="map"
        // onUpdate={(self) => (self.needsUpdate = true)}
        />} */}
      </meshBasicMaterial>
    </mesh>
  );
}

export const texConf = (tex: THREE.Texture, repeat: number) => {
  tex.wrapS = THREE.RepeatWrapping;
  tex.wrapT = THREE.RepeatWrapping;
  tex.anisotropy = 4;
  tex.repeat.set(repeat, repeat);
}

export default ({ texData, ...props }: any) => {

  // [texData] = useLoader(TextureLoader, [Assets.Images.Tiles087.getColor()]);
    // texConf(texData, 1);
    console.log(props)
    console.log(texData.image)
  return (
    // <Canvas
    //   // orthographic
    //   style={{
    //     width: texData.image.width,
    //     height: texData.image.height,
    //     border: "1px solid black",
    //     ...props.style
    //   }}
    // >
      <TextureFlatDisp texture={texData} {...props} />
    // {</Canvas> 
  );
}



