import * as THREE from "three"
import { useThree } from "@react-three/fiber";
import { useEffect } from "react";
import { Group, Vector3 } from "three"

export const GridHelper = ({ pos = new Vector3(0, 0, 0), size = 100, primaryDivs = 10, secondaryDivs = 100, labels = true }) => {
    const threeInstance = useThree();


    useEffect(() => {
        const grp = new Group()
        // grid helper
        var grid1 = new THREE.GridHelper(size, secondaryDivs, 0x888888);
        // grid1.material.color.setHex( 0x888888 );
        // grid1.material.vertexColors = false;
        grp.add(grid1);

        var grid2 = new THREE.GridHelper(size, primaryDivs, 0x222222);
        // grid2.material.color.setHex( 0x222222 );
        // grid2.material.depthFunc = THREE.AlwaysDepth;
        // grid2.material.vertexColors = false;
        grp.add(grid2);

        const geometry = new THREE.PlaneGeometry(1000, 1000);
        geometry.rotateX(- Math.PI / 2);
        const raycastPlane = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ visible: false }));
        grp.add(raycastPlane);

        // objects.push(raycastPlane);
        threeInstance.scene.add(grp);
    }, [])

    return (<>
    </>)
}