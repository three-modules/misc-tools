import { Canvas, useThree } from "@react-three/fiber";
import { useEffect } from "react";
import * as THREE from 'three'
import { Lut } from "three/examples/jsm/math/Lut";

export const LutWidget = ({ lut, cursorValue, style }: { lut: Lut, cursorValue: number, style: any }) => {
    let percent = ((cursorValue - lut.minV) / (lut.maxV - lut.minV)) * 100;

    // [texData] = useLoader(TextureLoader, [Assets.Images.Tiles087.getColor()]);
    // texConf(texData, 1);
    // orthoCamera = new THREE.OrthographicCamera( - 1, 1, 1, - 1, 1, 2 );
    // orthoCamera.position.set( 0.5, 0, 1 );
    return (<>
        <div style={{ position: "relative", display: "flex", flexFlow: "column", alignItems: "stretch" }}>
            <div style={{ textAlign: "center", fontWeight: "bold" }}> {lut.maxV + "m"} </div>
            {/* <div style={{ position: "absolute", top: 10 }}> */}
            <div style={{ position: "relative", height: "100%" }}>
                <Canvas
                    orthographic
                    style={{
                        // position: "absolute",
                        // top: 25,
                        ...style,
                        height: "100%",
                        width: 64
                    }}
                >
                    <Init />
                    <LutDisplay lut={lut} />
                </Canvas>
                <div style={{ textAlign: "center", fontWeight: "bold", position: "absolute", bottom: percent + "%" }}> &nbsp;&nbsp;&nbsp;&nbsp;{"-" + cursorValue + "-"} </div>
            </div>
            <div style={{ textAlign: "center", fontWeight: "bold" }}> {lut.minV + "m"} </div>
            {/* </div> */}
            {/* <div style={{ position: "absolute", bottom: "0" }}> <div>___^^^   </div> <div>   130   </div></div> */}
        </div >
    </>
    );
}

const Init = () => {
    const camera: any = useThree((state) => state.camera);
    const set = useThree((state) => state.set)

    useEffect(() => {
        const orthoCamera = new THREE.OrthographicCamera(- 1, 1, 1, - 1, 1, 2);
        orthoCamera.position.set(0, 0, 1);
        set({ camera: orthoCamera });
    }, [])

    const orthoCam: THREE.OrthographicCamera = camera;
    // orthoCam.position.set(0.5, 0, 1);
    // console.log(orthoCam);
    return (<></>)
}

const LutDisplay = ({ lut }) => {
    return (<sprite scale={[1, 2, 1]}>
        <spriteMaterial>
            <canvasTexture args={[lut.createCanvas()]} attach="map" />
        </spriteMaterial>
    </sprite>)
}

